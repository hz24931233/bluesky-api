package com.bluesky.admin.controller.system;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.bluesky.common.Constants;
import com.bluesky.common.dto.LoginDTO;
import com.bluesky.common.result.R;
import com.bluesky.framework.security.config.TenantContextHolder;
import com.bluesky.framework.web.service.IUserService;
import com.bluesky.system.common.vo.AccountInfoVO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 登录 前端控制器
 * </p>
 *
 * @author Kevin
 */
@RestController
public class LoginController {

    @Resource
    IUserService userService;

    @PostMapping("/login")
    public R login(@Validated @RequestBody LoginDTO req) {
        if (StrUtil.isBlank(req.getTenantCode())) {
            req.setTenantCode(Constants.ADMIN_TENANT);
        }
        TenantContextHolder.setCurrentTenant(req.getTenantCode());
        String token = userService.login(req);
        JSONObject result = new JSONObject();
        result.put("token", token);
        return R.success(result);
    }

    @GetMapping("/getUserInfo")
    public R getUserInfo() {
        return R.success(userService.getUserInfo());
    }

    @GetMapping("/getPermCode")
    public R getPermCode() {
        return R.success(userService.getPermCode());
    }

    @GetMapping("/getMenuList")
    public R getMenuList() {
        return R.success(userService.getMenuList());
    }

    @GetMapping("/getAccountInfo")
    public R getAccountInfo() {
        return R.success(userService.getAccountInfo());
    }

    @PostMapping("/saveAccountInfo")
    public R saveAccountInfo(@Validated @RequestBody AccountInfoVO req) {
        userService.saveAccountInfo(req);
        return R.success();
    }

}
