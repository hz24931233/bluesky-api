package com.bluesky.admin.controller;

import cn.hutool.extra.servlet.ServletUtil;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 首页 前端控制器
 * </p>
 *
 * @author Kevin
 */
@RestController
public class IndexController {

    private static void writeIndex(HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-type", "text/html; charset=UTF-8");
        ServletUtil.getWriter(response).write("<h1 style=\"text-align: center;padding: 50px 0;\">欢迎访问API</h1>");
    }

    @GetMapping("/")
    @SneakyThrows
    public void api(HttpServletResponse response) {
        writeIndex(response);
    }

    @GetMapping("/index")
    public void index(HttpServletResponse response) {
        writeIndex(response);
    }

}
