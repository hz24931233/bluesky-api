package com.bluesky.admin;

import com.bluesky.system.service.ISysConfigService;
import com.bluesky.system.service.ISysDictService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 项目初始化加载
 *
 * @author Kevin
 */
@Component
public class StartUpRunner implements CommandLineRunner {

    @Resource
    ISysDictService sysDictService;

    @Resource
    ISysConfigService sysConfigService;

    @Override
    public void run(String... args) {
        System.out.println("============ 服务器启动中....，开始加载数据 ============");
        sysDictService.loadAllDictCache();
        sysConfigService.loadAllConfig();
    }

}
