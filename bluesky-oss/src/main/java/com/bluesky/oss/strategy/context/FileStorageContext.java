package com.bluesky.oss.strategy.context;

import com.bluesky.common.config.AppConfig;
import com.bluesky.common.exception.CustomException;
import com.bluesky.oss.strategy.FileStorageStrategy;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 文件存储策略上下文
 *
 * @author Kevin
 */
@Component
public class FileStorageContext {

    Map<String, FileStorageStrategy> fileStorageStrategyMap = Maps.newHashMap();

    public FileStorageContext(List<FileStorageStrategy> list) {
        list.forEach(item -> {
            fileStorageStrategyMap.put(item.getStrategyName(), item);
        });
    }

    /**
     * 获取默认文件存储上下文
     *
     * @return
     */
    public FileStorageStrategy getContext() {
        return getContext(AppConfig.getUploadType());
    }

    /**
     * 获取文件存储上下文
     *
     * @param StrategyType
     * @return
     */
    public FileStorageStrategy getContext(String StrategyType) {
        FileStorageStrategy fileStorageStrategy = fileStorageStrategyMap.get(StrategyType.toUpperCase());
        if (Objects.isNull(fileStorageStrategy)) {
            throw new CustomException("文件存储方式暂不支持");
        }
        return fileStorageStrategy;
    }

}
