package com.bluesky.oss.strategy.impl;

import com.bluesky.common.enums.FileStorageStrategyEnum;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * MinIO文件存储策略
 *
 * @author Kevin
 */
@Service
public class AliyunStorageStrategy extends AbstractFileStorage {

    @Override
    public String getStrategyName() {
        return FileStorageStrategyEnum.ALIYUN.name();
    }


    @Override
    public String upload(MultipartFile file, String bizPath) {
        return null;
    }
}
