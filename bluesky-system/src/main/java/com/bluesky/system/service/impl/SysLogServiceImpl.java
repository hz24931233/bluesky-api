package com.bluesky.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bluesky.common.Constants;
import com.bluesky.common.util.SecurityUtils;
import com.bluesky.system.common.dto.SysLogQueryDTO;
import com.bluesky.system.entity.SysLog;
import com.bluesky.system.entity.SysRole;
import com.bluesky.system.entity.SysTenant;
import com.bluesky.system.mapper.SysLogMapper;
import com.bluesky.system.service.ISysLogService;
import com.bluesky.system.service.ISysTenantService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 操作日志表  服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

    @Resource
    private ISysTenantService sysTenantService;

    @Override
    public IPage<SysLog> page(Page reqPage, SysLogQueryDTO req) {
        LambdaQueryWrapper<SysLog> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(req.getTitle()), SysLog::getTitle, req.getTitle());
        queryWrapper.like(StrUtil.isNotBlank(req.getLogStatus()), SysLog::getLogStatus, req.getLogStatus());
        queryWrapper.like(StrUtil.isNotBlank(req.getUserPlatform()), SysLog::getUserPlatform, req.getUserPlatform());
        queryWrapper.like(StrUtil.isNotBlank(req.getOperName()), SysLog::getOperName, req.getOperName());
        if (!SecurityUtils.getTenantCode().equals(Constants.ADMIN_TENANT)) {
            queryWrapper.eq(SysLog::getTenantId, SecurityUtils.getTenantId());
        }
        queryWrapper.orderByDesc(SysLog::getCreateTime);
        IPage<SysLog> page = this.page(reqPage, queryWrapper);
        page.getRecords().forEach(item -> {
            // 设置租户名称
            SysTenant sysTenant = sysTenantService.getById(item.getTenantId());
            item.setTenantName(Objects.isNull(sysTenant) ? null : sysTenant.getTenantName());
        });
        return page;
    }

    @Override
    public SysLog view(String id) {
        return this.getById(id);
    }

}
