package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysLogQueryDTO;
import com.bluesky.system.entity.SysLog;

/**
 * <p>
 * 操作日志表  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
public interface ISysLogService extends IService<SysLog> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysLog> page(Page reqPage, SysLogQueryDTO req);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysLog view(String id);

}
