package com.bluesky.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysMenuAddDTO;
import com.bluesky.system.common.dto.SysMenuEditDTO;
import com.bluesky.system.common.dto.SysMenuQueryDTO;
import com.bluesky.system.entity.SysMenu;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单表  服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-19
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysMenu> page(Page reqPage, SysMenuQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysMenuAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysMenuEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysMenu view(String id);

    /**
     * 查询菜单树
     *
     * @return
     */
    List<Tree<String>> listMenuTree();

    /**
     * 查询下拉菜单树
     *
     * @return
     */
    List<Tree<String>> selectMenuTree();

    /**
     * 根据用户ID，查询权限标识
     *
     * @param userId
     * @return
     */
    Set<String> getPermissionsByUserId(Long userId);

    /**
     * 根据用户ID，查询菜单列表
     *
     * @param userId
     * @return
     */
    List<SysMenu> listGrantMenuByUserId(Long userId);

}
