package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysTenantAddDTO;
import com.bluesky.system.common.dto.SysTenantEditDTO;
import com.bluesky.system.common.dto.SysTenantQueryDTO;
import com.bluesky.system.entity.SysTenant;

import java.util.List;

/**
 * <p>
 * 租户 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-09-29
 */
public interface ISysTenantService extends IService<SysTenant> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysTenant> page(Page reqPage, SysTenantQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysTenant> list(SysTenantQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysTenantAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysTenantEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysTenant view(String id);

    /**
     * 下拉列表
     *
     * @return
     */
    List<SysTenant> select();

}
