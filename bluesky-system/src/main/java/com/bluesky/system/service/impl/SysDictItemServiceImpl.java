package com.bluesky.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bluesky.common.exception.CustomException;
import com.bluesky.system.common.dto.SysDictItemAddDTO;
import com.bluesky.system.common.dto.SysDictItemEditDTO;
import com.bluesky.system.common.dto.SysDictItemQueryDTO;
import com.bluesky.system.entity.SysDictItem;
import com.bluesky.system.mapper.SysDictItemMapper;
import com.bluesky.system.service.ISysDictItemService;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 字典项 服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements ISysDictItemService {

    @Override
    public IPage<SysDictItem> page(Page reqPage, SysDictItemQueryDTO req) {
        if (Objects.isNull(req.getDictId())) {
            IPage<SysDictItem> page = new Page<>();
            return page;
        }
        LambdaQueryWrapper<SysDictItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Objects.nonNull(req.getDictId()), SysDictItem::getDictId, req.getDictId());
        queryWrapper.like(StrUtil.isNotBlank(req.getDictItemCode()), SysDictItem::getDictItemCode, req.getDictItemCode());
        queryWrapper.like(StrUtil.isNotBlank(req.getDictItemName()), SysDictItem::getDictItemName, req.getDictItemName());
        queryWrapper.orderByAsc(SysDictItem::getSort);
        return this.page(reqPage, queryWrapper);
    }

    @Override
    public List<SysDictItem> list(SysDictItemQueryDTO req) {
        if (Objects.isNull(req.getDictId())) {
            return Lists.newArrayList();
        }
        LambdaQueryWrapper<SysDictItem> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(Objects.nonNull(req.getDictId()), SysDictItem::getDictId, req.getDictId());
        queryWrapper.like(StrUtil.isNotBlank(req.getDictItemCode()), SysDictItem::getDictItemCode, req.getDictItemCode());
        queryWrapper.like(StrUtil.isNotBlank(req.getDictItemName()), SysDictItem::getDictItemName, req.getDictItemName());
        queryWrapper.orderByAsc(SysDictItem::getSort);
        return this.list(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(SysDictItemAddDTO req) {
        if (!this.checkUniqueDictItemName(req.getDictItemName(), null, req.getDictId())) {
            throw new CustomException("字典项名称已存在");
        }
        if (!this.checkUniqueDictItemCode(req.getDictItemCode(), null, req.getDictId())) {
            throw new CustomException("字典项编码已存在");
        }
        SysDictItem entity = BeanUtil.copyProperties(req, SysDictItem.class);
        this.save(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(SysDictItemEditDTO req) {
        if (!this.checkUniqueDictItemName(req.getDictItemName(), req.getId(), req.getDictId())) {
            throw new CustomException("字典项名称已存在");
        }
        if (!this.checkUniqueDictItemCode(req.getDictItemCode(), req.getId(), req.getDictId())) {
            throw new CustomException("字典项编码已存在");
        }
        SysDictItem entity = BeanUtil.copyProperties(req, SysDictItem.class);
        this.updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(String ids) {
        List<String> idList = Arrays.asList(ids.split(","));
        this.removeByIds(idList);
    }

    @Override
    public SysDictItem view(String id) {
        return this.getById(id);
    }

    private Boolean checkUniqueDictItemName(String value, Long id, Long dictId) {
        if (StrUtil.isBlank(value)) {
            return true;
        }
        id = Objects.isNull(id) ? -1L : id;
        SysDictItem entity = this.getOne(Wrappers.lambdaQuery(SysDictItem.class).eq(SysDictItem::getDictItemName, value).eq(SysDictItem::getDictId, dictId));
        return Objects.isNull(entity) || entity.getId().longValue() == id.longValue();
    }

    private Boolean checkUniqueDictItemCode(String value, Long id, Long dictId) {
        if (StrUtil.isBlank(value)) {
            return true;
        }
        id = Objects.isNull(id) ? -1L : id;
        SysDictItem entity = this.getOne(Wrappers.lambdaQuery(SysDictItem.class).eq(SysDictItem::getDictItemCode, value).eq(SysDictItem::getDictId, dictId));
        return Objects.isNull(entity) || entity.getId().longValue() == id.longValue();
    }

}
