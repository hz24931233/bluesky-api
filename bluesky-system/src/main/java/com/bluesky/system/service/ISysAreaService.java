package com.bluesky.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysAreaQueryDTO;
import com.bluesky.system.entity.SysArea;

import java.util.List;

/**
 * <p>
 * 行政区域 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-08-08
 */
public interface ISysAreaService extends IService<SysArea> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysArea> page(Page reqPage, SysAreaQueryDTO req);


    /**
     * 查询区域树
     *
     * @return
     */
    List<Tree<String>> listAreaTree();

}
