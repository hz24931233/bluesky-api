package com.bluesky.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bluesky.system.entity.SysUserPost;
import com.bluesky.system.mapper.SysUserPostMapper;
import com.bluesky.system.service.ISysUserPostService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户岗位表 服务实现类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Service
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements ISysUserPostService {

}
