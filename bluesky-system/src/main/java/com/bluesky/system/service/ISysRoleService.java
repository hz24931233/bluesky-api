package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysRoleAddDTO;
import com.bluesky.system.common.dto.SysRoleAssignMenuDTO;
import com.bluesky.system.common.dto.SysRoleEditDTO;
import com.bluesky.system.common.dto.SysRoleQueryDTO;
import com.bluesky.system.entity.SysDept;
import com.bluesky.system.entity.SysMenu;
import com.bluesky.system.entity.SysRole;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysRole> page(Page reqPage, SysRoleQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysRole> list(SysRoleQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysRoleAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysRoleEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysRole view(String id);

    /**
     * 查询下拉框列表
     *
     * @return
     */
    List<SysRole> listOptions();

    /**
     * 分配菜单
     *
     * @param req
     */
    void assignMenu(SysRoleAssignMenuDTO req);

    /**
     * 查询角色关联的菜单
     *
     * @param roleId
     * @return
     */
    List<SysMenu> listRoleMenus(String roleId);

    /**
     * 查询角色关联的部门
     *
     * @param roleId
     * @return
     */
    List<SysDept> listRoleDepts(String roleId);

    /**
     * 根据用户ID，查询角色
     *
     * @param userId
     * @return
     */
    List<SysRole> listSysRoleByUserId(Long userId);

    /**
     * 下拉列表
     *
     * @return
     */
    List<SysRole> selectOptions(Long tenantId);

}
