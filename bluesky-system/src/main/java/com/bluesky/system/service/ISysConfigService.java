package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysConfigAddDTO;
import com.bluesky.system.common.dto.SysConfigEditDTO;
import com.bluesky.system.common.dto.SysConfigQueryDTO;
import com.bluesky.system.entity.SysConfig;

import java.util.List;

/**
 * <p>
 * 系统配置 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-23
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysConfig> page(Page reqPage, SysConfigQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysConfig> list(SysConfigQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysConfigAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysConfigEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysConfig view(String id);

    /**
     * 获取参数value
     *
     * @param key
     * @return
     */
    String getConfigValueByKey(String key);

    /**
     * 加载所有参数
     */
    void loadAllConfig();

}
