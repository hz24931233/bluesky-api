package com.bluesky.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bluesky.system.common.dto.SysDictItemAddDTO;
import com.bluesky.system.common.dto.SysDictItemEditDTO;
import com.bluesky.system.common.dto.SysDictItemQueryDTO;
import com.bluesky.system.entity.SysDictItem;

import java.util.List;

/**
 * <p>
 * 字典项 服务类
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface ISysDictItemService extends IService<SysDictItem> {

    /**
     * 分页查询
     *
     * @param reqPage
     * @param req
     * @return
     */
    IPage<SysDictItem> page(Page reqPage, SysDictItemQueryDTO req);

    /**
     * 查询列表
     *
     * @param req
     * @return
     */
    List<SysDictItem> list(SysDictItemQueryDTO req);

    /**
     * 新增
     *
     * @param req
     */
    void add(SysDictItemAddDTO req);

    /**
     * 修改
     *
     * @param req
     */
    void edit(SysDictItemEditDTO req);

    /**
     * 删除
     *
     * @param ids
     */
    void remove(String ids);

    /**
     * 查看
     *
     * @param id
     * @return
     */
    SysDictItem view(String id);

}
