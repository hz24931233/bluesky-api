package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.system.common.dto.SysAreaQueryDTO;
import com.bluesky.system.service.ISysAreaService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 行政区域 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-08-08
 */
@RestController
@RequestMapping("/system/sysArea")
public class SysAreaController {


    @Resource
    private ISysAreaService sysAreaService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysArea:page')")
    @Log(title = "行政区域分页查询")
    public R page(Page reqPage, SysAreaQueryDTO req) {
        return R.success(sysAreaService.page(reqPage, req));
    }

    /**
     * 查询区域树
     */
    @GetMapping("/listAreaTree")
    public R listAreaTree() {
        return R.success(sysAreaService.listAreaTree());
    }

}

