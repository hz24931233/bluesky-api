package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.system.common.dto.SysMenuAddDTO;
import com.bluesky.system.common.dto.SysMenuEditDTO;
import com.bluesky.system.common.dto.SysMenuQueryDTO;
import com.bluesky.system.service.ISysMenuService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 菜单表  前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-19
 */
@RestController
@RequestMapping("/system/sysMenu")
public class SysMenuController {

    @Resource
    private ISysMenuService sysMenuService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysMenu:page')")
    @Log(title = "菜单管理分页查询")
    public R page(Page reqPage, SysMenuQueryDTO req) {
        return R.success(sysMenuService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysMenu:add')")
    @Log(title = "菜单管理新增")
    public R add(@Validated @RequestBody SysMenuAddDTO req) {
        sysMenuService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysMenu:edit')")
    @Log(title = "菜单管理修改")
    public R edit(@Validated @RequestBody SysMenuEditDTO req) {
        sysMenuService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysMenu:remove')")
    @Log(title = "菜单管理删除")
    public R remove(@RequestParam String ids) {
        sysMenuService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysMenu:view')")
    @Log(title = "菜单管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysMenuService.view(id));
    }

    /**
     * 查询菜单树
     */
    @GetMapping("/listMenuTree")
    public R listMenuTree() {
        return R.success(sysMenuService.listMenuTree());
    }


    /**
     * 查询下拉菜单树
     */
    @GetMapping("/selectMenuTree")
    public R selectMenuTree() {
        return R.success(sysMenuService.selectMenuTree());
    }

}

