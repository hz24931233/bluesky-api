package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.system.common.dto.SysTenantAddDTO;
import com.bluesky.system.common.dto.SysTenantEditDTO;
import com.bluesky.system.common.dto.SysTenantQueryDTO;
import com.bluesky.system.entity.SysTenant;
import com.bluesky.system.service.ISysTenantService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 租户 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-09-29
 */
@RestController
@RequestMapping("/system/sysTenant")
public class SysTenantController {

    @Resource
    private ISysTenantService sysTenantService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:page')")
    @Log(title = "租户分页查询")
    public R page(Page reqPage, SysTenantQueryDTO req) {
        return R.success(sysTenantService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:add')")
    @Log(title = "租户新增")
    public R add(@Validated @RequestBody SysTenantAddDTO req) {
        sysTenantService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:edit')")
    @Log(title = "租户修改")
    public R edit(@Validated @RequestBody SysTenantEditDTO req) {
        sysTenantService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:remove')")
    @Log(title = "租户删除")
    public R remove(@RequestParam String ids) {
        sysTenantService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:view')")
    @Log(title = "租户查看")
    public R view(@RequestParam String id) {
        return R.success(sysTenantService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysTenant:export')")
    @Log(title = "租户导出")
    public R export(SysTenantQueryDTO req) {
        String filepath = ExcelUtils.export("租户列表", SysTenant.class, sysTenantService.list(req));
        return R.success(filepath);
    }

    /**
     * 下拉列表
     */
    @GetMapping("/select")
    public R select() {
        return R.success(sysTenantService.select());
    }

}

