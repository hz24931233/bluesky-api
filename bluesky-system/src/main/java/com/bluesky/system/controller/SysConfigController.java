package com.bluesky.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bluesky.common.annotation.Log;
import com.bluesky.common.result.R;
import com.bluesky.common.util.ExcelUtils;
import com.bluesky.system.common.dto.SysConfigAddDTO;
import com.bluesky.system.common.dto.SysConfigEditDTO;
import com.bluesky.system.common.dto.SysConfigQueryDTO;
import com.bluesky.system.entity.SysConfig;
import com.bluesky.system.service.ISysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 系统配置 前端控制器
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@RestController
@RequestMapping("/system/sysConfig")
public class SysConfigController {

    @Resource
    private ISysConfigService sysConfigService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:page')")
    @Log(title = "参数管理分页查询")
    public R page(Page reqPage, SysConfigQueryDTO req) {
        return R.success(sysConfigService.page(reqPage, req));
    }

    /**
     * 新增
     */
    @PostMapping("/add")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:add')")
    @Log(title = "参数管理新增")
    public R add(@Validated @RequestBody SysConfigAddDTO req) {
        sysConfigService.add(req);
        return R.success();
    }

    /**
     * 修改
     */
    @PostMapping("/edit")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:edit')")
    @Log(title = "参数管理修改")
    public R edit(@Validated @RequestBody SysConfigEditDTO req) {
        sysConfigService.edit(req);
        return R.success();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:remove')")
    @Log(title = "参数管理删除")
    public R remove(@RequestParam String ids) {
        sysConfigService.remove(ids);
        return R.success();
    }

    /**
     * 查看
     */
    @GetMapping("/view")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:view')")
    @Log(title = "参数管理查看")
    public R view(@RequestParam String id) {
        return R.success(sysConfigService.view(id));
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:export')")
    @Log(title = "参数管理导出")
    public R export(SysConfigQueryDTO req) {
        String filepath = ExcelUtils.export("参数列表", SysConfig.class, sysConfigService.list(req));
        return R.success(filepath);
    }

    /**
     * 根据键值，获取value
     */
    @GetMapping("/getConfigValue")
    public R getConfigValue(@RequestParam String key) {
        return R.success(sysConfigService.getConfigValueByKey(key));
    }

    /**
     * 刷新缓存
     */
    @PostMapping("/refresh")
    @PreAuthorize("@ss.hasPermission('system:sysConfig:refresh')")
    @Log(title = "参数管理刷新缓存")
    public R refresh() {
        sysConfigService.loadAllConfig();
        return R.success();
    }

}

