package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * DTO模板类，方便复制该类，创建新类
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseTemplateDTO implements Serializable {

}
