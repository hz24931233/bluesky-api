package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
@Data
@Accessors(chain = true)
public class SysLogQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;

    /**
     * 日志状态 0成功；1失败
     */
    private String logStatus;

    /**
     * 用户平台 WEB：web管理后台；APP：前端用户平台
     */
    private String userPlatform;

    /**
     * 操作人员
     */
    private String operName;


}