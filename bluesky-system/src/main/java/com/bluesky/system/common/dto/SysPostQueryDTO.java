package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 岗位表
 * </p>
 *
 * @author Kevin
 * @since 2021-06-22
 */
@Data
@Accessors(chain = true)
public class SysPostQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 岗位编码
     */
    private String postCode;

    /**
     * 岗位名称
     */
    private String postName;


}