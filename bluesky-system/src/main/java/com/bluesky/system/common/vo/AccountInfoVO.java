package com.bluesky.system.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 账户信息VO
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AccountInfoVO implements Serializable {

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    @Size(max = 100, message = "昵称长度不能超过100个字符")
    private String nickname;

    /**
     * 姓名
     */
    @Size(max = 100, message = "姓名长度不能超过100个字符")
    private String realname;

    /**
     * 英文名
     */
    @Size(max = 100, message = "英文名长度不能超过100个字符")
    private String englishName;

    /**
     * 头像
     */
    @Size(max = 100, message = "头像长度不能超过100个字符")
    private String avatar;

    /**
     * 邮箱
     */
    @Size(max = 100, message = "邮箱长度不能超过100个字符")
    private String email;

    /**
     * 性别 1男；2女；3未知
     */
    @NotBlank(message = "性别不能为空")
    private String sex;

    /**
     * 备注
     */
    @Size(max = 500, message = "备注长度不能超过500个字符")
    private String remarks;

}
