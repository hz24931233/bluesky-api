package com.bluesky.system.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 路由项VO
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RouteItemVO implements Serializable {

    private String path;

    private String component;

    private RouteMetoVO meta;

    private String name;

    private String alias;

    private String redirect;

    private Boolean caseSensitive;

    private List<RouteItemVO> children;

}
