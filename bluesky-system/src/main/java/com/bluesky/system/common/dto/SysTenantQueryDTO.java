package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 租户表
 * </p>
 *
 * @author Kevin
 * @since 2021-09-29
 */
@Data
@Accessors(chain = true)
public class SysTenantQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 租户编码
     */
    private String tenantCode;

    /**
     * 租户名称
     */
    private String tenantName;

}