package com.bluesky.system.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统参数
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
@Data
@Accessors(chain = true)
public class SysConfigQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 参数名称
     */
    private String configName;

    /**
     * 参数键名
     */
    private String configKey;


}