package com.bluesky.system.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * VO模板类，方便复制该类，创建新类
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseTemplateVO implements Serializable {

}
