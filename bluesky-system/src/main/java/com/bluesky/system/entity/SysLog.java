package com.bluesky.system.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author Kevin
 * @since 2021-07-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 租户ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long tenantId;

    /**
     * 标题
     */
    private String title;

    /**
     * 日志状态 0成功；1失败
     */
    private String logStatus;

    /**
     * 用户平台 WEB：web管理后台；APP：前端用户平台
     */
    private String userPlatform;

    /**
     * 请求地址
     */
    private String requsetUri;

    /**
     * 请求方式
     */
    private String requsetType;

    /**
     * 请求方法
     */
    private String requsetMethod;

    /**
     * 请求参数
     */
    private String requsetParams;

    /**
     * 返回参数
     */
    @JsonIgnore
    private String responseResult;

    /**
     * 请求耗时
     */
    private String requsetTime;

    /**
     * 异常信息
     */
    @JsonIgnore
    private String exception;

    /**
     * 操作人员
     */
    private String operName;

    /**
     * IP地址
     */
    private String ipAddress;

    /**
     * 操作地点
     */
    private String operLocation;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 租户名称
     */
    @TableField(exist = false)
    private String tenantName;

}