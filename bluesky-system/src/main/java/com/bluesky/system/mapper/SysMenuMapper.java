package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysMenu;

/**
 * <p>
 * 菜单表  Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-19
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
