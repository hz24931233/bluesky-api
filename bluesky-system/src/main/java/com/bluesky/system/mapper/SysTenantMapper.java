package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysTenant;

/**
 * <p>
 * 租户表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-09-29
 */
public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
