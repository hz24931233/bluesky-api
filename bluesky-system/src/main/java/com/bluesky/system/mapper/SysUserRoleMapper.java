package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysUserRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-06-10
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
