package com.bluesky.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bluesky.system.entity.SysRoleMenu;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author Kevin
 * @since 2021-07-05
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
