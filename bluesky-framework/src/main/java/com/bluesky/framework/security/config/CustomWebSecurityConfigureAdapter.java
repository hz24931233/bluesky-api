package com.bluesky.framework.security.config;

import com.bluesky.framework.security.config.mobile.MobileCodeAuthenticationProvider;
import com.bluesky.framework.security.config.username.UsernamePasswordAuthenticationProvider;
import com.bluesky.framework.security.filter.AuthTokenFilter;
import com.bluesky.framework.security.filter.MyCorsFilter;
import com.bluesky.framework.security.handler.CustomAccessDeniedHandler;
import com.bluesky.framework.security.handler.CustomAuthenticationEntryPoint;
import com.bluesky.framework.security.handler.CustomLogoutSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

import javax.annotation.Resource;


/**
 * 自定义WebSecurityConfigurerAdapter
 *
 * @author Kevin
 */
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class CustomWebSecurityConfigureAdapter extends WebSecurityConfigurerAdapter {

    @Resource
    private CustomAuthenticationEntryPoint authenticationEntryPoint;

    @Resource
    private CustomAccessDeniedHandler accessDeniedHandler;

    @Resource
    private CustomLogoutSuccessHandler logoutSuccessHandler;

    @Resource
    private UsernamePasswordAuthenticationProvider usernamePasswordAuthenticationProvider;

    @Resource
    private MobileCodeAuthenticationProvider mobileCodeAuthenticationProvider;

    @Resource
    private AuthTokenFilter authTokenFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(mobileCodeAuthenticationProvider)
                .authenticationProvider(usernamePasswordAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                .authenticationEntryPoint(authenticationEntryPoint)
                // 基于token，所以不需要session
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // 授权配置
                .authorizeRequests()
                // 登录，允许所有人访问
                .antMatchers("/login").permitAll()
                // 项目首页，允许所有人访问
                .antMatchers("/", "/index").permitAll()
                // common，允许所有人访问
                .antMatchers("/common/**").permitAll()
                // 拦截所有请求
                .anyRequest()
                // 需要认证
                .authenticated()
                .and().logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler).permitAll();
        httpSecurity.addFilterBefore(authTokenFilter, UsernamePasswordAuthenticationFilter.class);
        // 添加CORS filter
        httpSecurity.addFilterBefore(new MyCorsFilter(), CorsFilter.class);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
