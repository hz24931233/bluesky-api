package com.bluesky.framework.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


/**
 * 自定义UserDetails接口
 *
 * @author Kevin
 */
public interface ICustomUserDetailsService extends UserDetailsService {

    /**
     * 手机号登录
     *
     * @param mobile
     * @return
     * @throws UsernameNotFoundException
     */
    UserDetails loadUserByMobile(String mobile) throws UsernameNotFoundException;

}
