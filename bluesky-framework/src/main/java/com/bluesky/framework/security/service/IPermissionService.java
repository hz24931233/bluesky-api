package com.bluesky.framework.security.service;

/**
 * 权限service
 *
 * @author Kevin
 */
public interface IPermissionService {

    /**
     * 判断接口是否有xxx:xxx权限
     *
     * @param permission
     * @return
     */
    boolean hasPermission(String permission);

}
