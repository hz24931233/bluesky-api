package com.bluesky.framework.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * 校验框架配置类
 *
 * @author Kevin
 */
@Configuration
public class ValidatorConfig {

    /**
     * 配置校验框架 快速返回模式
     * <p>
     * Spring Validation默认会校验完所有字段，然后才抛出异常。
     * 可以通过一些简单的配置，开启Fali Fast模式，一旦校验失败就立即返回。
     */
    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        return validatorFactory.getValidator();
    }

}
