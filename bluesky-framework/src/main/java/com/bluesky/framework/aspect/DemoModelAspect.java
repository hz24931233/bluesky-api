package com.bluesky.framework.aspect;

import com.bluesky.common.config.AppConfig;
import com.bluesky.common.exception.DemoModeException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 演示环境AOP
 *
 * @author Kevin
 */
@Aspect
@Component
@Slf4j
public class DemoModelAspect {

    @Pointcut("execution(public * com.bluesky..*.controller..*(..))")
    public void pointcut() {
    }

    @Before("pointcut()")
    public void before() {
        HttpServletRequest requset = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (AppConfig.isDemoEnabled()
                && "POST".equals(requset.getMethod())
                && !requset.getRequestURI().contains("/login")) {
            throw new DemoModeException();
        }
    }

}
