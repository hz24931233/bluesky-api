package com.bluesky.framework.web.service;

import com.bluesky.common.bo.LoginUser;

/**
 * Token接口类
 *
 * @author Kevin
 */
public interface ITokenService {

    /**
     * 创建token
     *
     * @param user
     * @return
     */
    String createToken(LoginUser user);

    /**
     * 检查token是否有效
     *
     * @param token
     * @return
     */
    boolean checkToken(String token);

    /**
     * 清除token
     *
     * @param token
     */
    void deleteToken(String token);

    /**
     * 根据token获取用户
     *
     * @param token
     * @return
     */
    LoginUser getUserByToken(String token);

}
