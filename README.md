<div align="center">
<h1>BlueSky Admin</h1>
</div>

<p align="center">
    <img src="https://img.shields.io/badge/Spring%20Boot-2.5.2-blue.svg" alt="Downloads">
    <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="Build Status">
    <img src="https://img.shields.io/badge/license-Apache%202-blue.svg" alt="Build Status">
</p>

### 简介

BlueSky Admin权限管理系统，基于VUE3.x、SpringBoot2.x、SpringSecurity、MyBatis-Plus等技术实现的前后端分离的权限管理系统。 可用于学习参考和项目开发。

### 特性

- WEB容器使用了undertow，相较于tomcat，并发性更好，性能要好一些。
- Lombok，消除冗长的java代码，更加简化。
- Mybatis Plus，可以简化CRUD开发。
- Mybatis Plus Generator，生成前后端代码，简化开发工作量。
- 使用java新特性，Stream API、lambda表达式等。
- Hutool工具集合，减少项目工具类的编写。
- Spring Security，通过自定义Provider，实现用户名密码登录和手机号验证码两种登录模式。
- Spring Security权限，细分到页面按钮级别。
- EasyExcel，方便导入导出功能，自定义Convert类，实现了数据字典的转化。
- Guava，非常方便的java工具集，提供了类似Lists.newArrayList()和Sets.newHashSet()等静态方法。
- DataSource注解，支持多数据源切换。
- Fastjson，方便了JSON的格式化和解析。
- Alibaba Java Coding Guidelines插件，IDEA插件，提高代码质量。
- MinIO，分布式文件存储。
- 前端框架采用最新技术栈，Vue3 & Vite，打包更快更轻。
- 前端框架采用TypeScript和Eslint，规范代码，提高项目可持续性和可维护性。

### 多租户
<table>
    <tr>
        <td>用户账号</td>
        <td>描述</td>
        <td>角色编码</td>
        <td>描述</td>
    </tr>
    <tr>
        <td>admin【id=1】</td>
        <td>超级管理员</td>
        <td>无</td>
        <td>无</td>
    </tr>
    <tr>
        <td>admin</td>
        <td>租户管理员</td>
        <td>admin</td>
        <td>租户管理员</td>
    </tr>
</table>

- 超级管理员，在租户管理模块，新增租户信息，并自动为租户创建租户管理员角色【admin】和租户管理员用户【admin】
- 超级管理员，在角色管理模块，为租户管理员角色【admin】分配菜单
- 菜单集合有A|B|C|D|E，超级管理员为租户管理员角色【admin】分配菜单A|C|D
- 租户管理员角色【admin】只能看到菜单A|C|D，租户再为自己的子角色分配菜单，以及创建角色和用户
- 前后端代码，待优化【TODO】

### 演示环境

演示地址：[http://skysong.gitee.io/bluesky-ui](http://skysong.gitee.io/bluesky-ui)

### 预览

<table>
    <tr>
        <td>
            <img src="./doc/images/1.png"/>
        </td>
        <td>
            <img src="./doc/images/2.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/3.png"/>
        </td>
        <td>
            <img src="./doc/images/4.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/5.png"/>
        </td>
        <td>
            <img src="./doc/images/6.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/7.png"/>
        </td>
        <td>
            <img src="./doc/images/8.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/9.png"/>
        </td>
        <td>
            <img src="./doc/images/10.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/11.png"/>
        </td>
        <td>
            <img src="./doc/images/12.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/13.png"/>
        </td>
        <td>
            <img src="./doc/images/14.png"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="./doc/images/15.png"/>
        </td>
        <td>
            <img src="./doc/images/16.png"/>
        </td>
    </tr>
</table>

### 项目地址

- [bluesky-ui](https://gitee.com/skysong/bluesky-ui) - 前端UI
- [bluesky-api](https://gitee.com/skysong/bluesky-api) - 后端API

### 项目结构

```
bluesky-api
├── bluesky-admin -- 内置功能，后台管理
├── bluesky-codegen -- 内置功能，代码生成
├── bluesky-common --内置功能，通用工具
├── bluesky-framework -- 内置功能，核心模块
├── bluesky-system -- 内置功能，系统模块
├── bluesky-oss -- 内置功能，OSS文件存储模块
```

### 核心依赖

| 依赖                   | 版本          |
| ---------------------- | ------------ |
| Spring Boot            | 2.5.4        |
| Spring Boot Security   | 2.5.4        |
| Mybatis Plus           | 3.4.3.3      |
| Mybatis Plus Generator | 3.4.1        |
| Hutool                 | 5.7.16       |
| Guava                  | 30.1.1-jre   |
| EasyExcel              | 2.2.11       |
| Fastjson               | 1.2.78       |
| Minio                  | 8.3.3       |

### 内置功能

```
1、菜单管理
2、字典管理
3、部门管理
4、岗位管理
5、角色管理
6、用户管理
7、参数设置
8、行政区域
9、租户管理
10、在线用户
11、操作日志
12、账户设置
```

### 前端安装

```
# 安装依赖
pnpm install

# 运行项目
pnpm serve

# 打包发布
pnpm build

```

### 前端说明

- [vue-vben-admin](https://gitee.com/annsion/vue-vben-admin) - 基于该前端框架开发，不定期同步更新。

### 环境安装

```
1、安装Mysql数据库，安装Redis，安装MinIO文件存储。
2、执行./doc/db/schema.sql，创建数据库。
3、执行./doc/db/bluesky.sql，创建数据表和插入基础数据。
```

### Docker常用命令

```
1、查看镜像
docker images

2、查看所有容器
docker ps -a

3、拉取镜像
docker pull redis:latest

4、参数解释
-- name，设置运行的镜像名称
-p，映射端口 ，虚拟机端口 :docker端口 
-e，设置环境变量
-v，挂载目录/文件，虚拟机目录/文件:docker目录/文件
--privileged=true，设置特权，比如为mysql获取root权限
-d，守护进程后台运行
-it，启动并运行
--restart=always，在docker服务重启后,自动重启mysql服务,也可以把docker 服务作为开机启动，这样mysql就可以跟着开机启动了
--link，设置容器别名
```

### Docker安装Redis

```
1、拉取镜像
docker pull redis:latest

2、启动并运行
docker run -itd --name redis -p 6379:6379 redis
```

### Docker安装Mysql

```
1、拉取镜像
docker pull mysql:5.7

2、创建目录并授权
mkdir -p /opt/docker/mysql/data /opt/docker/mysql/conf
chmod -R 777 /opt/docker

3、创建配置文件并授权
touch /opt/docker/mysql/conf/my.cnf
vi /opt/docker/mysql/conf/my.cnf
chmod 777 /opt/docker/mysql/conf/my.cnf
my.cnf内容如下：
[client]     
port=3306   
default-character-set=utf8
[mysql]   
default-character-set=utf8
[mysqld]
character_set_server=utf8
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
lower_case_table_names=1

4、启动并运行
docker run -itd --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 --privileged=true -v /opt/docker/mysql/conf/my.cnf:/etc/mysql/my.cnf -v /opt/docker/mysql/data:/var/lib/mysql -v /opt/docker/mysql/logs:/var/log/mysql mysql:5.7
```

### Docker安装MinIO

```
1、拉取镜像
docker pull minio/minio

2、创建目录并授权
mkdir -p /opt/docker/minio/data
chmod -R 777 /opt/docker

3、启动并运行
docker run -d -p 9000:9000 -p 9001:9001 --name=minio -v /opt/docker/minio/data:/data quay.io/minio/minio server /data/data-{1...4} --console-address ":9001" --address ":9000"
```

### MinIO文件存储

<table>
    <tr>
        <td>
            <img src="./doc/images/minio/minio1.png"/>
        </td>
        <td>
            <img src="./doc/images/minio/minio2.png"/>
        </td>
    </tr>
</table>

### 部署

```
一、打包命令：指定prod环境，进行打包
    mvn clean package -DskipTests -Pprod
二、启动
    1、Windows环境，运行./doc/bin/run.bat
    2、Linux环境，运行./doc/bin/linux/startup.sh
三、注意事项
    1、Linux执行脚本，需要先授权，chmod +x startup.sh
    2、run.bat或startup.sh，需要和bluesky-admin.jar放在同一个目录运行
    3、指定端口，需要修改脚本，并添加server.port参数，示例：--server.port=9090
四、服务器部署目录结构
    /opt
    ├── bluesky
    ├──── bluesky-ui
    ├──── bluesky-api
    ├────── startup.sh
    ├────── bluesky-admin.jar
```

### nginx配置

```
    location / {
        root   /opt/bluesky/bluesky-ui;
        try_files $uri $uri/ @router;
        index  index.html index.htm;
    }
    
    location @router {
        rewrite ^.*$ /index.html last;
    }
    
    location ^~ /api {
        proxy_pass  http://localhost:9090/api;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
```

### 使用帮助

#### 代码同步方法（示例）

1、fork代码，fork from https://gitee.com/annsion/vue-vben-admin <br/>
2、创建自己的分支，进行开发，feature-dev <br/>
3、码云强制同步 <br/>
<img src="./doc/images/doc/20210716151749.png"/>
<br/>
4、合并代码，merge into feature-dev <br/>
<img src="./doc/images/doc/20210716152152.png"/>

#### 代码生成（CodeGenerator）

```
    /** 按照个人需要，进行修改 */
    public static final String AUTHOR = "Kevin";
    public static final String PROJECT_PATH = "D:\\tempCode";
    public static final String PACKAGE_PARENT = "com.bluesky";
    public static final String MODULE_NAME = "system";

    /** 生成SQL脚本的上级菜单的ID，要开发的功能，需要放到XXX菜单下面，请找到XXX菜单的ID */
    public static final String PARENT_MENU_ID = "1406064334403878913";

    /** admin的ID，可以不用修改 */
    public static final String CREATE_BY = "1";
    public static final String UPDATE_BY = "1";

    /** 默认菜单图标，可以不用修改，SQL脚本生成之后，在页面选择图标，进行修改即可 */
    public static final String ICON = "ant-design:unordered-list-outlined";

    public static void main(String[] args) {
        new CodeGenerator().generate(
                //"sys_tenant"
        );
    }
```

#### EasyExcel使用

```
excel标题宽度
两个字：@ColumnWidth(10)
四个字：@ColumnWidth(15)
```

#### Java编码规范（Java开发手册更新至嵩山版）

[Java开发手册](./doc/java开发手册/阿里巴巴Java开发手册（嵩山版）.pdf)

#### Vue编码规范

```
1、所有的Component文件都是以大写开头 (PascalCase)

2、所有的.js文件都遵循驼峰命名 (CamelCase)

3、在views文件下，代表路由的.vue文件都使用驼峰命名 (CamelCase)，代表路由的文件夹也是使用同样的规则

4、api下模块和view下模块，一一对应
```

#### CSS编码规范

```
CSS编码规范，BEM：就是块（block）、元素（element）、修饰符（modifier）

.block{}
.block__element{}
.block--modifier{}

.block 代表了更高级别的抽象或组件。
.block__element 代表.block的后代，用于形成一个完整的.block的整体。
.block--modifier代表.block的不同状态或不同版本
```

### QQ群，问题反馈和Bug修复

805450144