package com.bluesky.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取Minio相关配置
 *
 * @author Kevin
 */
@Component
@ConfigurationProperties(prefix = "minio")
@Data
public class MinioConfig {

    private String endpoint;

    private String accessKey;

    private String secretKey;

    private String bucketName;

}
