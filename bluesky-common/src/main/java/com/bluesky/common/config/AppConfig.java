package com.bluesky.common.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author Kevin
 */
@Component
@ConfigurationProperties(prefix = "app")
@Getter
public class AppConfig {

    /**
     * 项目名称
     */
    @Getter
    private static String name;

    /**
     * 实例演示开关
     */
    @Getter
    private static boolean demoEnabled = false;

    /**
     * 获取地址开关
     */
    @Getter
    private static boolean addressEnabled = false;

    /**
     * 上传目录
     */
    @Getter
    private static String uploadType;

    /**
     * 上传目录
     */
    @Getter
    private static String uploadDir;

    /**
     * 缓存前缀
     */
    @Getter
    private static String cachePrefix;

    /**
     * 令牌有效期
     */
    @Getter
    private static Integer expireTime;

    public void setName(String name) {
        AppConfig.name = name;
    }

    public void setDemoEnabled(boolean demoEnabled) {
        AppConfig.demoEnabled = demoEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled) {
        AppConfig.addressEnabled = addressEnabled;
    }

    public void setUploadType(String uploadType) {
        AppConfig.uploadType = uploadType;
    }

    public void setUploadDir(String uploadDir) {
        AppConfig.uploadDir = uploadDir;
    }

    public void setCachePrefix(String cachePrefix) {
        AppConfig.cachePrefix = cachePrefix;
    }

    public void setExpireTime(Integer expireTime) {
        AppConfig.expireTime = expireTime;
    }
}
