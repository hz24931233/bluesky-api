package com.bluesky.common.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.excel.EasyExcel;
import com.bluesky.common.config.AppConfig;
import com.bluesky.common.enums.BizEnum;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Excel工具类
 *
 * @author Kevin
 */
@Slf4j
public class ExcelUtils {

    public static String export(String filename, Class clazz, List list) {
        return export(filename, clazz, list, filename);
    }

    public static String export(String filename, Class clazz, List list, String sheetName) {
        filename = filename + "_" + IdUtil.fastSimpleUUID() + ".xlsx";
        String filepath = "/" + BizEnum.TEMP.getCode() + "/" + DateUtil.today() + "/" + filename;
        String absoluteFilepath = AppConfig.getUploadDir() + filepath;
        log.info("导出文件路径，{}", absoluteFilepath);
        FileUtil.touch(absoluteFilepath);
        EasyExcel.write(absoluteFilepath, clazz).head(clazz).sheet(sheetName).doWrite(list);
        return filepath;
    }

}
