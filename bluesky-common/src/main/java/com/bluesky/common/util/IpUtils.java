package com.bluesky.common.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * IP获取工具
 *
 * @author Kevin
 */
public class IpUtils {

    public static String getClientIP(HttpServletRequest request) {
        // natapp穿透工具搭建的环境，通过header，X-Natapp-Ip，获取
        String ipAddress = ServletUtil.getClientIPByHeader(request, "X-Natapp-Ip");
        if (StrUtil.isBlank(ipAddress)) {
            ipAddress = ServletUtil.getClientIP(request);
        }
        // 本地开发，客户端和服务器在同一台机器，获取到是0:0:0:0:0:0:0:1，ip6地址，需要进行转换
        ipAddress = "0:0:0:0:0:0:0:1".equals(ipAddress) ? "127.0.0.1" : ipAddress;
        return ipAddress;
    }

}
