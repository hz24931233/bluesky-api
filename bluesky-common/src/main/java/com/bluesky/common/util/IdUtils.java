package com.bluesky.common.util;

import cn.hutool.core.lang.Singleton;
import cn.hutool.core.lang.Snowflake;

/**
 * ID生成器工具类
 *
 * @author Kevin
 */
public class IdUtils {
    /**
     * 雪花算法，采用系统时钟
     * https://blog.csdn.net/weixin_42444592/article/details/109643200
     *
     * @return 随机UUID
     */
    public static Long getSnowflakeId() {
        return Singleton.get(Snowflake.class, 0L, 0L, true).nextId();
    }
}
