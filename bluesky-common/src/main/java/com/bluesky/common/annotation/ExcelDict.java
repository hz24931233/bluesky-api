package com.bluesky.common.annotation;

import java.lang.annotation.*;

/**
 * 数据字典注解
 *
 * @author Kevin
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ExcelDict {

    /**
     * 字典编码
     *
     * @return
     */
    String value() default "";

}
