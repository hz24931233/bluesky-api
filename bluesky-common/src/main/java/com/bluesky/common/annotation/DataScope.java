package com.bluesky.common.annotation;

import java.lang.annotation.*;

/**
 * 数据权限注解
 *
 * @author Kevin
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataScope {

    /**
     * 表别名
     *
     * @return
     */
    String tableAlias() default "t";

    /**
     * 部门字段名
     *
     * @return
     */
    String deptColumnName() default "dept_id";

    /**
     * 用户字段名
     *
     * @return
     */
    String userColumnName() default "create_by";

}
