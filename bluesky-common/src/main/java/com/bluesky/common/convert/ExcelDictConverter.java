package com.bluesky.common.convert;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import com.bluesky.common.annotation.ExcelDict;
import com.bluesky.common.util.DictUtils;

import java.lang.reflect.Field;

/**
 * Excel数据字典转换器
 *
 * @author Kevin
 */
public class ExcelDictConverter implements Converter<String> {

    @Override
    public Class supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public String convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        Field field = excelContentProperty.getField();
        String dictCode = AnnotationUtil.getAnnotationValue(field, ExcelDict.class);
        String dictValue = DictUtils.getDictValue(dictCode, cellData.getStringValue());
        return StrUtil.isBlank(dictValue) ? cellData.getStringValue() : dictValue;
    }

    @Override
    public CellData convertToExcelData(String s, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        Field field = excelContentProperty.getField();
        String dictCode = AnnotationUtil.getAnnotationValue(field, ExcelDict.class);
        String dictLabel = DictUtils.getDictLabel(dictCode, s);
        return new CellData(StrUtil.isBlank(dictLabel) ? s : dictLabel);
    }

}
