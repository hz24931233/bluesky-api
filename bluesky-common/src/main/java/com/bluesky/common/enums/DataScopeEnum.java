package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据范围 1全部数据权限；2自定义数据权限；3本部门数据权限；4本部门及以下数据权限；5仅本人数据权限
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnum {

    /**
     * 全部数据权限
     */
    ALL("1", "全部数据权限"),
    /**
     * 自定义数据权限
     */
    CUSTOM("2", "自定义数据权限"),
    /**
     * 本部门数据权限
     */
    DEPT("3", "本部门数据权限"),
    /**
     * 不内嵌
     */
    DEPT_AND_CHILD("4", "本部门及以下数据权限"),
    /**
     * 不内嵌
     */
    SELF("5", "仅本人数据权限");

    private String code;
    private String desc;
}
