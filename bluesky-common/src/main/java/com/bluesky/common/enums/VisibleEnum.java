package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否显示
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum VisibleEnum {

    /**
     * 显示
     */
    SHOW("0", "显示"),
    /**
     * 隐藏
     */
    HIDE("1", "隐藏");

    private String code;
    private String desc;

}
