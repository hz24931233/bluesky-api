package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 删除标记
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum DelFlagEnum {

    /**
     * 存在
     */
    NO("0", "存在"),
    /**
     * 删除
     */
    YES("1", "删除");

    private String code;
    private String desc;

}
