package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 业务类型
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum BizEnum {
    /**
     * 临时文件
     */
    TEMP("temp", "临时文件"),
    /**
     * 图片
     */
    IMAGE("image", "图片"),
    /**
     * Excel
     */
    EXCEL("excel", "Excel");

    private String code;
    private String desc;

}
