package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 修改标记
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum PswModifiedEnum {

    /**
     * 未修改
     */
    NO("0", "未修改"),
    /**
     * 已修改
     */
    YES("1", "已修改");

    private String code;
    private String desc;

}
