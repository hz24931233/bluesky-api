package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 授权类型
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum GrantTypeEnum {

    /**
     * 用户名密码模式
     */
    USERNAME_PASSWORD("1", "用户名密码模式"),
    /**
     * 手机号短信模式
     */
    MOBILE_CODE("2", "手机号短信模式");

    private String code;
    private String desc;


    public static Boolean contains(String code) {
        return Arrays.stream(GrantTypeEnum.values()).anyMatch(temp -> temp.getCode().equals(code));
    }

}
