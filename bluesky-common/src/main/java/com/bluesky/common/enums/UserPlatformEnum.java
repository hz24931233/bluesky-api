package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 用户平台
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum UserPlatformEnum {

    /**
     * web管理后台
     */
    WEB("WEB", "web管理后台"),
    /**
     * 前端用户平台
     */
    APP("APP", "前端用户平台");

    private String code;
    private String desc;

}
