package com.bluesky.common.enums;

/**
 * 文件存储策略
 *
 * @author Kevin
 */
public enum FileStorageStrategyEnum {

    /**
     * MinIO文件存储
     */
    MINIO,

    /**
     * Aliyun文件存储
     */
    ALIYUN;

}
