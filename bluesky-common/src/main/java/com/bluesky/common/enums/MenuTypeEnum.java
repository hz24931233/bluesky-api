package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 菜单类型
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum MenuTypeEnum {

    /**
     * 目录
     */
    DIR("dir", "目录"),
    /**
     * 菜单
     */
    MENU("menu", "菜单"),
    /**
     * 按钮
     */
    BUTTON("button", "按钮");

    private String code;
    private String desc;

}
