package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 是否唯一
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum UniqueEnum {
    /**
     * 否
     */
    NO("0", "否"),
    /**
     * 是
     */
    YES("1", "是");

    private String code;
    private String desc;

}
