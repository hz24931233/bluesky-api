package com.bluesky.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 锁定标记
 *
 * @author Kevin
 */
@Getter
@AllArgsConstructor
public enum LockFlagEnum {

    /**
     * 正常
     */
    NORMAL("0", "正常"),
    /**
     * 锁定
     */
    LOCKED("1", "锁定");

    private String code;
    private String desc;

}
