package com.bluesky.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录实体类
 *
 * @author Kevin
 */
@Data
public class LoginDTO {

    String tenantCode;

    @NotBlank(message = "授权类型不能为空")
    String grantType;

    String username;
    String password;

    String mobile;
    String code;

}
