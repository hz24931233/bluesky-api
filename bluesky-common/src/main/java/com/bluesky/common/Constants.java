package com.bluesky.common;

import com.bluesky.common.config.AppConfig;

/**
 * 常量类
 *
 * @author Kevin
 */
public class Constants {

    /**
     * header token
     */
    public static final String HEADER_TOKEN = "Authorization";

    /**
     * admin tenant code
     */
    public static final String ADMIN_TENANT = "0000";

    /**
     * redis缓存，项目统一前缀
     */
    public static final String REDIS_PREFIX = AppConfig.getCachePrefix() + ":";
    /**
     * redis缓存，用户token缓存模板
     */
    public static final String USER_TOKEN_TPL = REDIS_PREFIX + "user_token:%s";
    /**
     * redis缓存，系统字典缓存模板
     */
    public static final String SYS_DICT_TPL = REDIS_PREFIX + "sys_dict:%s";
    /**
     * redis缓存，系统参数缓存模板
     */
    public static final String SYS_CONFIG_TPL = REDIS_PREFIX + "sys_config:%s";
    /**
     * 分钟 * 60s
     */
    public static final long USER_TOKEN_EXPIRE = AppConfig.getExpireTime().intValue() * 60;

}
