package com.bluesky.common.exception;

import lombok.Data;

/**
 * 自定义异常
 *
 * @author Kevin
 */
@Data
public class CustomException extends RuntimeException {

    private Integer code;

    private String message;

    public CustomException(String message) {
        this.message = message;
    }
}
