package com.bluesky.common.bo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色
 *
 * @author Kevin
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class SysRoleBO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 数据范围 1全部数据权限；2自定数据权限；3本部门数据权限；4本部门及以下数据权限
     */
    private String dataScope;

}
