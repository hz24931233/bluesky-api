UPDATE sys_area
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_config
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_dept
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_dict
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_dict_item
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_log
SET create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_menu
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_post
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_role
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_tenant
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

UPDATE sys_user
SET create_by = '1',
    update_by = '1',
    create_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' ),
    update_time = STR_TO_DATE( '2020-10-01 00:00:00', '%Y-%m-%d %H:%i:%s' );

DELETE FROM sys_log;